

import turtle as tr
import math as m
import numpy as np

print("Choose an initial velocity and initial angle for a missile.")
print("This program will detect a collision point and shoot a second")
print("missile to blow the other one out of the sky")
print()
print("Velocity:")
print("1. 7500 mph")
print("2. 8500 mph")
print("3. 9500 mph")
vnum = input("Choose an initial velocity(1,2,3): ")
print()
print("Angle:")
print("1. 35 Degrees")
print("2. 45 Degrees")
print("3. 55 Degrees")
anum = input("Choose an initial angle(1,2,3): ")
print()

if vnum=="1":
    initVel = 75
elif vnum=="2":
    initVel = 85
else:
    initVel = 95

if anum=="1":
    initAng = 35
elif anum=="2":
    initAng = 45
else:
    initAng = 55

#find the width and height of the turtle angle
w = tr.window_width()
h = tr.window_height()

#uses the initial velocity and angle to find the x and y points
def getPoints(n,a,v):
    points = []
    #gather information every .1 seconds for n total seconds
    for t in np.arange(0.0,n,0.1):
        x = v*t*m.cos(m.radians(a))
        y = v*t*m.sin(m.radians(a))-0.5*9.81*t**2.0
        points.append(x)
        points.append(y)
    return points

#create the grid in the background
def createGrid():
    turt2 = tr.Turtle()
    turt2.speed(0)
    turt2.penup()
    turt2.goto(w,h)
    turt2.pen(pencolor="gray")
    for x in np.arange(w/2.0,-w/2.0,-10.0):
        turt2.pendown()
        turt2.goto(x,h/2.0)
        turt2.goto(x,-h/2.0)
        turt2.penup()
        turt2.goto(x-10.0,-h/2.0)


misPoints = getPoints(15.0,initAng,initVel)
#make the x and y lists to hold the data points
xList=[]
yList=[]
#add the points that do not have a negative y
#since we don't want to account for when the missile
#goes past the ground.
for p in range(0,len(misPoints),2):
    if misPoints[p+1]>=0:
        xList.append(misPoints[p])
        yList.append(misPoints[p+1])

#get the polynomial formula for the first 30 points
formula = np.polyfit(xList[0:30],yList[0:30],2)
a = formula[0]
b = formula[1]
c = formula[2]

##find the angle of missile 1 at the moment missile 2 launches
#we call this the detection point
da = m.tanh((yList[30]-yList[29])/(xList[30]-xList[29]))
#convert angle to degrees
da = (da*180)/(m.pi)

##find the velocity of missile 1 at the moment missile 2 launches
#first you find the time at the 30th point, the detection point
t = xList[30]/(initVel*m.cos(m.radians(initAng)))
#find vy of the initial velocity for missile 1
vy = m.sin(m.radians(initAng))*initVel
#find the y component of the velocity at the detection point
dvy = m.sqrt((2*(-9.81)*yList[30])+(vy**2))
#find the x component of the velocity at the detection point
dxy = m.sin(m.radians(initAng))*initVel
#find the velocity of the missile at the detection point
dv = dvy/m.sin(m.radians(da))

##find the time to the mid-air collision
#we set the mid-air collision at 200 points past the detection point
collX = xList[30] + xList[30]
time = (collX-xList[30])/(dv*m.cos(m.radians(da)))

##find the initial x and y velocity for missile 2
Vx2 = (w-collX)/time
Vy2 = ((a*collX**2+b*collX+c)-(.5*-9.81*(time**2)))/time

##find the angle that the second missile fires from
ang2 = m.degrees(m.tanh(Vy2/Vx2))

##find the initial velocity of the second missile
v2 = Vy2/(m.sin(m.radians(ang2)))


##create the turtle diagram
#import and set the images
screen = tr.Screen()
expl = "expl.gif"
screen.addshape(expl)
screen.bgpic("desertBackground.gif")

#make the grid
createGrid()

#make the x and y lists to hold the data points
misPoints2 = getPoints(15.0,ang2,v2)
xList2=[]
yList2=[]
#add the points that do not have a negative y
#since we don't want to account for when the missile
#goes past the ground.
for p in range(0,len(misPoints2),2):
    if misPoints2[p+1]>=0:
        xList2.append(misPoints2[p])
        yList2.append(misPoints2[p+1])

#move turtle to appropriate starting point for missile 1
tr.goto(-w / 2, 0)
tr.speed(1)
tr.setheading(initAng)

#move a new turtle to the starting point of missile 2
mis2 = tr.Turtle()
mis2.goto(w/2,0)
mis2.speed(1)
mis2.setheading(180-da)

#draw the path of the first missile up until the detection point
tr.pen(pencolor = "red")
for p in range(0,len(xList[0:30])):
    tr.goto(xList[p]-w/2,yList[p])

    #direction missile is facing
    if p>0:
        dir = m.tanh((yList[p] - yList[p-1]) / (xList[p] - xList[p-1]))
        tr.setheading(m.degrees(dir))

#remove the points up unitl the collision point for the first
#missile
xList = xList[30:]
yList = yList[30:]

#continue with missile one's trajectory and fire missile 2
mis2.pen(pencolor = "red")
for p in range(0,len(xList)):
    tr.goto(xList[p]-w/2,yList[p])
    mis2.goto(-xList2[p]+w/2,yList2[p])

    # direction missiles are facing
    if p>0:
        dir = m.tanh((yList[p] - yList[p - 1]) / (xList[p] - xList[p - 1]))
        tr.setheading(m.degrees(dir))
        dir2 = m.tanh((yList2[p] - yList2[p - 1]) / (xList2[p] - xList2[p - 1]))
        mis2.setheading(180-m.degrees(dir2))

    #stop when they hit in air
    if yList[p]<=yList2[p]:
        tr.shape(expl)
        mis2.shape(expl)
        break

tr.exitonclick()